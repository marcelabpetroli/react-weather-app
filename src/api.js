export const geoApiOptions = {
  method: "GET",
  headers: {
    "X-RapidAPI-Key": "180269d63bmshf92eb6de4583495p11671ajsnec5c21f6936b",
    "X-RapidAPI-Host": "wft-geo-db.p.rapidapi.com",
  },
};
export const GEO_API_URL = "https://wft-geo-db.p.rapidapi.com/v1/geo";

export const WEATHER_API_URL = "https://api.openweathermap.org/data/2.5";

export const WEATHER_API_KEY = "727abada27bdb62a70f6343d90cd7323";
